using UnityEngine;
using System.Collections.Generic;

[RequireComponent (typeof(CharacterController))]
[RequireComponent (typeof(CharacterMotor))]
public class CharacterCollider : MonoBehaviour {
	public InventoryManager manager;
	public InventoryContainer container;
	
	public RestartManager restartManager;
	
	private CharacterController controller;
	private CharacterMotor motor;
	
	void Start() {
		controller = GetComponent<CharacterController>();
		motor = GetComponent<CharacterMotor>();
	}
	
	void OnControllerColliderHit(ControllerColliderHit hit) {
		CheckHeadCollision();
		if(hit.collider.tag.Equals("UnColoredBlock"))
			hit.collider.GetComponent<BlockColorChanger>().Score();
		if(hit.collider.tag.Equals("Restart")) {
			restartManager.Restart();
		}
	}
	
	private void CheckHeadCollision() {
		Ray ray = new Ray(transform.position + (transform.up * controller.height) - (transform.up * 0.1f), transform.up);
		RaycastHit hit;
		
		if(Physics.Raycast(ray, out hit, 0.2f)) {
			if(hit.collider.tag.Equals("UnColoredBlock"))
				hit.collider.GetComponent<BlockColorChanger>().Score();
			motor.HeadOnCeiling();
		}
	}
	
	public void SpecializeUnderneath() {
		Ray ray = new Ray(transform.position + (transform.up * 0.1f), -transform.up);
		RaycastHit hit;
		
		if(Physics.Raycast(ray, out hit, 0.2f)) {
			if(hit.collider.tag.Equals("ColoredBlock")) {
				if(container.inventorySlots[manager.currentlySelected].quantity > 0) {
					hit.collider.GetComponent<BlockColorChanger>().Specialize(manager.currentlySelected);
					container.inventorySlots[manager.currentlySelected].Remove();
				}
			}
		}
	}
}