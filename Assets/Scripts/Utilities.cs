using UnityEngine;
using System.Collections;

public static class Utilities {
	public static string TimerToString(float timer) {
		int seconds = (int)timer;
		int milliseconds = (int)((timer - seconds) * 1000);
		int minutes = seconds / 60;
		seconds = seconds % 60;
		
		string toReturn = minutes + ":";
		if(seconds < 10)
			toReturn += "0";
		toReturn += seconds + ".";
		if(milliseconds < 100)
			toReturn += "0";
		if(milliseconds < 10)
			toReturn += "0";
		toReturn += milliseconds;
		
		return toReturn;
	}
}
