using UnityEngine;
using System.Collections;

public class VolumeAdjuster : MonoBehaviour {
	void OnLevelWasLoaded(int level) {
		Object[] srcs = GameObject.FindObjectsOfType(typeof(AudioSource));
		foreach (AudioSource src in srcs) {
			if(src.clip)
				src.volume = PlayerPrefs.GetInt("music", 100) / 100f;
		}
		
		Object[] cms = GameObject.FindObjectsOfType(typeof(CharacterMotor));
		foreach (CharacterMotor cm in cms)
			cm.SetVolume(PlayerPrefs.GetInt("sound", 100) / 100f);
	}
}
