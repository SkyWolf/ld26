using UnityEngine;
using System.Collections;

public class LevelSelector : MonoBehaviour {
	private int toFocus = 0;
	
	private bool release = true;
	private BlockTypeInitializer bti;
	private LoadProperties lp;
	
	public GUISkin desiredSkin;
	
	private Vector2 scrollPosition = Vector2.zero;
	
	private float buttonHeight;
	
	void Start() {
		GameObject globalVariables = GameObject.FindGameObjectWithTag("Global Variables");
		bti = globalVariables.GetComponent<BlockTypeInitializer>();
		lp = globalVariables.GetComponent<LoadProperties>();
	}
	
	void Update() {
		if(Input.GetAxis("Vertical") < 0) {
			if(release) {
				toFocus = (toFocus + 1) % (lp.totalNumberOfLevels + 1);
				release = false;
				
				if(toFocus < lp.totalNumberOfLevels)
					scrollPosition = new Vector2(0, toFocus * buttonHeight);
			}
		}
		else if(Input.GetAxis("Vertical") > 0) {
			if(release) {
				toFocus--;
				if(toFocus < 0)
					toFocus = lp.totalNumberOfLevels;
				release = false;
				
				if(toFocus < lp.totalNumberOfLevels)
					scrollPosition = new Vector2(0, toFocus * buttonHeight);
			}
		}
		else
			release = true;
		
		if(Input.GetButtonDown("Jump")) {
			if(toFocus == lp.totalNumberOfLevels)
				MainMenu();
			else if(toFocus >= 0)
				LoadLevel(toFocus);
		}
		else if(Input.GetButtonDown("Main")) {
			MainMenu();
		}
	}
	
	void OnGUI() {
		GUI.skin = desiredSkin;
		
		Rect mainMenuBox = new Rect(	(Screen.width / 2) - (0.2f * Screen.width),
											0.3f * Screen.height,
											0.4f * Screen.width,
											0.4f * Screen.height );
		
		buttonHeight = GUI.skin.button.CalcHeight(GUIContent.none, mainMenuBox.width);

		GUILayout.BeginArea(mainMenuBox);
		scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true);
		for(int i = 0; i < lp.totalNumberOfLevels; i++) {
			string levelName;
			if(i <= PlayerPrefs.GetInt("latterUnlock", 0))
				levelName = bti.levels[i].name;
			else
				levelName = "LOCKED";
			
			GUI.SetNextControlName("A" + i.ToString());
			
			if(GUILayout.Button(levelName))
				LoadLevel(i);
		}
		
		GUILayout.EndScrollView();
		
		GUI.SetNextControlName("A" + lp.totalNumberOfLevels.ToString());
		if(GUILayout.Button("Back to the Main Menu"))
			MainMenu();
		
		GUILayout.EndArea();
		
		if(mainMenuBox.Contains(Input.mousePosition))
			toFocus = -1;
		GUI.FocusControl("A" + toFocus.ToString());
	}
	
	private void LoadLevel(int i) {
		if(i <= PlayerPrefs.GetInt("latterUnlock", 0)) {
			lp.levelToLoadNext = i;
			Application.LoadLevel("adjustGlobal");
		}
	}
	
	private void MainMenu() {
		Application.LoadLevel("mainMenu");
	}
}
