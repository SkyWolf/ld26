using UnityEngine;
using System.Collections.Generic;

/* WARNING
 * THIS CLASS SHOULD BE REFACTORED BIG TIME
 * IT'S TURNING INTO A GOD CLASS DUE TO SLEEP DEPRIVATION XD */

[RequireComponent (typeof(CharacterController))]
[RequireComponent (typeof(CharacterCollider))]
public class CharacterMotor : MonoBehaviour {
	private CharacterController controller;
	private CharacterCollider ccollider;
	public GameObject mesh;
	
	public Dictionary<string, AudioClip> clipsD;
	public AudioClip[] clips;
	
	public float characterGravity;
	
	private Vector3 lateralMovement;
	private Vector3 oldLateralMovement;
	private float lateralMovementStopCounter;
	
	private bool speedified;
	private float savedLateralSpeed;
	public float lateralSpeed;
	public float lateralStoppingTime;
	
	private bool canJump;
	
	private float verticalVelocity;
	private float oldVerticalVelocity;
	
	public float jumpSpeed;
	private float savedJumpSpeed;
	
	private float currentDirection;
	private int lastTurn;
	
	private int verticalMultiplier = 1;
	private bool multiplierJustChanged = false;
	
	private float oneShotVolume = 1;
	
	public GameObject inventory;
	
	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController>();
		ccollider = GetComponent<CharacterCollider>();
		
		canJump = false;
		
		savedLateralSpeed = lateralSpeed;
		savedJumpSpeed = jumpSpeed;
		speedified = false;
		
		lastTurn = 0;
		
		currentDirection = Mathf.Sign(1);
		
		clipsD = new Dictionary<string, AudioClip>();
		foreach(AudioClip ac in clips)
			clipsD.Add(ac.name, ac);
		
		mesh.animation["Turn Left"].speed = 1.5f;
	}
	
	// Update is called once per frame
	void Update () {
		ApplySpecial();
		Vector3 forwardMomentum = Vector3.zero;
		
		float direction = Input.GetAxis("Horizontal");
		if(lastTurn != 0)
			direction = Mathf.Abs(direction) * currentDirection;
		forwardMomentum += direction * Vector3.right * lateralSpeed;
		
		if(direction > 0) {
			if(Mathf.Sign(direction) != currentDirection) {
				lastTurn = 1;
				mesh.animation.CrossFade("Turn Left");
				currentDirection = Mathf.Sign(direction);
			}
		} else if(direction < 0) {
			if(Mathf.Sign(direction) != currentDirection) {
				lastTurn = -1;
				mesh.animation.CrossFade("Turn Left");
				currentDirection = Mathf.Sign(direction);
			}
		}
		
		if(!mesh.animation.isPlaying) {
			mesh.transform.RotateAroundLocal(Vector3.up, Mathf.PI);
			lastTurn = 0;
		}
		
		if(forwardMomentum != Vector3.zero) {
			oldLateralMovement = lateralMovement = forwardMomentum;
			
			if(lastTurn == 0)
				mesh.animation.CrossFade("Run", 0.2f);
			
			lateralMovementStopCounter = 0;
		}
		else {
			lateralMovementStopCounter += Time.deltaTime;
			float percentage = lateralMovementStopCounter / lateralStoppingTime;
			
			lateralMovement = Vector3.Lerp(oldLateralMovement, Vector3.zero, percentage);
			
			if(lastTurn == 0)
				mesh.animation.CrossFade("Idle", 0.2f);
		}
		
		if(!speedified && lateralMovement.magnitude < savedLateralSpeed)
			lateralSpeed = savedLateralSpeed;
		
		//Colorify blocks
		if(Input.GetButtonDown("Plant Seed")) {
			ccollider.SpecializeUnderneath();
		}
		
		//Gravity stuff
		if(Input.GetButtonDown("Jump") && canJump) {
			verticalVelocity = verticalMultiplier * jumpSpeed;
			canJump = false;
			
			audio.PlayOneShot(clipsD["jump"], oneShotVolume);
		}
				
		if(IsGrounded()) {
			if(verticalMultiplier * verticalVelocity < 0) {
				verticalVelocity = verticalMultiplier * characterGravity * Time.deltaTime;
				canJump = true;
			}
		}
		else {
			verticalVelocity += verticalMultiplier * characterGravity * Time.deltaTime;
		}
		
		//Total velocity
		Vector3 totalVelocity = lateralMovement;
		totalVelocity.y += verticalVelocity;
			
		controller.Move(totalVelocity * Time.deltaTime);
	}
	
	public void HeadOnCeiling() {
		if(verticalMultiplier * verticalVelocity > 0)
			verticalVelocity = 0;	
	}
	
	public bool IsGrounded() {
		Ray ray = new Ray(transform.position + (transform.up * 0.015f), -transform.up);
		RaycastHit hit;
		
		if(Physics.Raycast(ray, out hit, 0.03f))
			return true;
		return false;
	}
	
	public void ApplySpecial() {
		Ray ray = new Ray(transform.position + (transform.up * 0.1f), -transform.up);
		RaycastHit hit;
		
		if(Physics.Raycast(ray, out hit, 0.2f)) {
			if(hit.collider.tag.Equals("Special-SpeedBlock")) {
				Speedify(true);
			}
			else if(hit.collider.tag.Equals("Special-JumpBlock")) {
				//RESET
				speedified = false;
				jumpSpeed = savedJumpSpeed;
				multiplierJustChanged = false;
				
				jumpSpeed = savedJumpSpeed * 2;
			}
			else if(hit.collider.tag.Equals("Special-GravityBlock")) {
				ChangeGravity(hit);
			}
			else if(hit.collider.tag.Equals("Special-FallBlock")) {
				//RESET
				speedified = false;
				jumpSpeed = savedJumpSpeed;
				multiplierJustChanged = false;
				
				GameObject fallBlock = hit.collider.gameObject;
				if(!fallBlock.rigidbody) {
					fallBlock.AddComponent(typeof(Rigidbody));
					fallBlock.rigidbody.constraints = 	RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ |
														RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
				}
				
			}
			else {
				//RESET
				speedified = false;
				jumpSpeed = savedJumpSpeed;
				multiplierJustChanged = false;
			}
		}
	}
	
	private void Speedify(bool activate) {
		//RESET
		jumpSpeed = savedJumpSpeed;
		multiplierJustChanged = false;	
		
		if(activate) {
			if(!speedified) {
				audio.PlayOneShot(clipsD["speed"], oneShotVolume);
				lateralSpeed = savedLateralSpeed * 2;
				speedified = true;
			}
		}
		else if(speedified)
			speedified = false;
	}
	
	private void ChangeGravity(RaycastHit hit) {
		if(!multiplierJustChanged) {
			//RESET
			speedified = false;
			jumpSpeed = savedJumpSpeed;
			
			audio.PlayOneShot(clipsD["gravity"], oneShotVolume);
			
			verticalMultiplier *= -1;
			transform.RotateAround(transform.right, Mathf.PI);
			
			Vector3 newPosition = transform.position;
			newPosition += Vector3.up * verticalMultiplier * (controller.height - ((BoxCollider)hit.collider).size.y + 0.01f);
			newPosition.x = hit.collider.transform.position.x;
			transform.position = newPosition;
			
			inventory.transform.RotateAround(inventory.transform.up, Mathf.PI);
			inventory.GetComponent<InventoryManager>().rotationModifier = verticalMultiplier == 1 ? 0 : 180;
			
			multiplierJustChanged = true;
		}
	}
	
	public void SetVolume(float volume) {
		oneShotVolume = volume;
	}
}
