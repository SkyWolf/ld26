using UnityEngine;
using System.Collections;

[System.Serializable]
public class BlockType {
	public string name;
	public string tag;
	public Material material;
}