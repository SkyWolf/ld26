using UnityEngine;
using System.Collections;

public class BlockColorChanger : MonoBehaviour {	
	private BlockTypeInitializer bti;
	public Material coloredBlock;
	
	private Renderer mesh;
	
	void Start() {
		if(!bti || !mesh)
			Initialize();
	}
	
	public void Score() {
		if(!mesh)
			Initialize();
		
		mesh.material = coloredBlock;
		tag = "ColoredBlock";
	}
	
	public void Specialize(int index) {
		if(!bti || !mesh)
			Initialize();
		
		mesh.material = bti.blockTypes[index].material;
		tag = bti.blockTypes[index].tag;
	}
	
	private void Initialize() {
		bti = GameObject.FindGameObjectWithTag("Global Variables").GetComponent<BlockTypeInitializer>();
		mesh = transform.FindChild("Mesh").renderer;
	}
}
