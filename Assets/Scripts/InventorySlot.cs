using UnityEngine;
using System.Collections;

[System.Serializable]
public class InventorySlot {
	public BlockType block { get; private set; }
	public float quantity { get; private set; }
	
	public InventorySlot(BlockType _block, float _quantity) {
		block = _block;
		quantity = _quantity;
	}
	
	public void Remove() {
		quantity--;
	}
}
