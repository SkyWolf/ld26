using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	private const int NUMBER_OF_OBJECTS = 4;
	private int toFocus = 0;
	
	private bool release = true;
	private LoadProperties lp;
	
	public AudioSource sample;
	public AudioClip sampleClip;
	
	public GUISkin desiredSkin;
	
	void Start() {
		lp = GameObject.FindGameObjectWithTag("Global Variables").GetComponent<LoadProperties>();
	}
	
	void Update() {
		if(Input.GetAxis("Vertical") < 0) {
			if(release) {
				toFocus = (toFocus + 1) % NUMBER_OF_OBJECTS;
				release = false;
			}
		}
		else if(Input.GetAxis("Vertical") > 0) {
			if(release) {
				toFocus--;
				if(toFocus < 0)
					toFocus = NUMBER_OF_OBJECTS - 1;
				release = false;
			}
		}
		else
			release = true;
		
		if(Input.GetButtonDown("Jump")) {
			switch(toFocus) {
				case 0:	
					Play();
					break;
				case 1:	
					LevelSelector();
					break;
				case 2:	
					ChangeMusicLevel();
					break;
				case 3:	
					ChangeSoundLevel();
					break;
			}
		}
	}
	
	void OnGUI() {
		GUI.skin = desiredSkin;
		
		Rect mainMenuBox = new Rect(	(Screen.width / 2) - (0.2f * Screen.width),
											0.3f * Screen.height,
											0.4f * Screen.width,
											0.4f * Screen.height );
		GUILayout.BeginArea(mainMenuBox);
		
		GUILayout.BeginVertical();
		GUI.SetNextControlName("A0");
		if(GUILayout.Button("Play from the beginning"))
			Play();
		GUILayout.FlexibleSpace();
		GUI.SetNextControlName("A1");
		if(GUILayout.Button("Select a level"))
			LevelSelector();
		GUILayout.FlexibleSpace();
		GUI.SetNextControlName("A2");
		if(GUILayout.Button("Music: " + PlayerPrefs.GetInt("music", 100)))
			ChangeMusicLevel();
		GUI.SetNextControlName("A3");
		if(GUILayout.Button("Sound: " + PlayerPrefs.GetInt("sound", 100)))
			ChangeSoundLevel();
		GUILayout.EndVertical();
		GUILayout.EndArea();
		
		if(mainMenuBox.Contains(Input.mousePosition))
			toFocus = -1;
		GUI.FocusControl("A" + toFocus.ToString());
	}
	
	private void Play() {
		lp.levelToLoadNext = 0;
		Application.LoadLevel("adjustGlobal");
	}
	
	private void LevelSelector() {
		Application.LoadLevel("levelselector");	
	}
	
	private void ChangeMusicLevel() {
		int newValue = (PlayerPrefs.GetInt("music", 100) + 10) % 110;
		PlayerPrefs.SetInt("music", newValue);
		sample.volume = newValue / 100f;
	}
	
	private void ChangeSoundLevel() {
		int newValue = (PlayerPrefs.GetInt("sound", 100) + 10) % 110;
		PlayerPrefs.SetInt("sound", newValue);
		audio.PlayOneShot(sampleClip, newValue / 100f);
	}
}
