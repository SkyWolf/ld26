using UnityEngine;
using System.Collections;

public class TimeScoreManager : MonoBehaviour {
	public GUIText missingSquares;
	public GUIText elapsedTime;
	
	private int nMissingSquares;
	private float nElapsedTime;
	
	private LoadProperties lp;
	
	// Use this for initialization
	void Start () {
		lp = GameObject.FindGameObjectWithTag("Global Variables").GetComponent<LoadProperties>();
		nElapsedTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		nMissingSquares = GameObject.FindGameObjectsWithTag("UnColoredBlock").Length;
		missingSquares.text = "Missing Squares: " + nMissingSquares;
		
		if(nMissingSquares == 0) {
			int levelToUnlock = lp.levelToLoadNext + 1;
			if(levelToUnlock < lp.totalNumberOfLevels && levelToUnlock > PlayerPrefs.GetInt("latterUnlock", 0))
				PlayerPrefs.SetInt("latterUnlock", levelToUnlock);
			
			lp.previousScore = nElapsedTime;	
			Application.LoadLevel("score");
		}
		
		nElapsedTime += Time.deltaTime;
		elapsedTime.text = "Elapsed Time: " + Utilities.TimerToString(nElapsedTime);
	}
}