using UnityEngine;
using System.Collections;

[RequireComponent (typeof(InventoryContainer))]
public class InventoryManager : MonoBehaviour {
	private InventoryContainer container;
	
	public int currentlySelected { get; private set; }
	private int oldCurrentlySelected;
	
	public float animationTime;
	private float timeCollector;
	
	private Vector3 rotator;
	
	public float rotationModifier;
	
	// Use this for initialization
	void Start () {
		container = GetComponent<InventoryContainer>();
		currentlySelected = 0;
		
		timeCollector = 0;
		
		rotator = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		if (rotator != Vector3.zero) {
			timeCollector += Time.deltaTime;
			transform.localRotation = Quaternion.Euler(	0,
														rotationModifier + Mathf.SmoothStep(	oldCurrentlySelected * (360 / container.inventorySlots.Length),
																								currentlySelected * (360 / container.inventorySlots.Length),
																								timeCollector / animationTime	),
														0);
			
			if(timeCollector >= animationTime)
				rotator = Vector3.zero;
		}
				
		if(Input.GetButtonDown("Select Previous Seed")) {
			SelectPrevious();
		}
		else if(Input.GetButtonDown("Select Next Seed")) {
			SelectNext();
		}
	}
	
	public void SelectNext() {
		if(rotator == Vector3.zero) {
			rotator = Vector3.up;
			oldCurrentlySelected = currentlySelected;
			currentlySelected = (currentlySelected + 1) % container.inventorySlots.Length;
			
			timeCollector = 0;
		}
	}
	
	public void SelectPrevious() {
		if(rotator == Vector3.zero) {
			rotator = -Vector3.up;
			oldCurrentlySelected = currentlySelected;
			currentlySelected--;
			if(currentlySelected < 0)
				currentlySelected = container.inventorySlots.Length - 1;
			
			timeCollector = 0;
		}
	}
}
