using UnityEngine;
using System.Collections;

[RequireComponent (typeof(InventoryContainer))]
[RequireComponent (typeof(InventoryManager))]
public class InventoryVisual : MonoBehaviour {
	public GameObject block;
	public TextMesh counter;
	
	public float size;
	public float distance;
	
	private InventoryContainer container;
	private InventoryManager manager;
	
	// Use this for initialization
	void Start () {
		container = GetComponent<InventoryContainer>();
		manager = GetComponent<InventoryManager>();
		
		for(int i = 0; i < container.inventorySlots.Length; i++) {
			GameObject b = (GameObject)Instantiate(block, transform.position, Quaternion.identity);

			b.AddComponent<LocalRotationReset>();
			
			b.GetComponent<BlockColorChanger>().Specialize(i);
			
			b.collider.enabled = false;
			
			b.transform.FindChild("Mesh").renderer.castShadows = false;
			b.transform.FindChild("Mesh").renderer.receiveShadows = false;
			
			b.transform.Translate(transform.forward * -1 * distance);
			
			b.transform.RotateAround(transform.position, transform.up, (360 / container.inventorySlots.Length) * (container.inventorySlots.Length - i));
			b.transform.localScale = b.transform.localScale * size;
			b.transform.parent = transform;
		}
		
		UpdateTextMesh();
	}
	
	void Update() {
		UpdateTextMesh();	
	}
	
	private void UpdateTextMesh() {
		counter.text = container.inventorySlots[manager.currentlySelected].quantity + "";
	}
}
