using UnityEngine;
using System.Collections;

public class GameLoader : MonoBehaviour {
	private LoadProperties lp;
	private BlockTypeInitializer bti;
	
	private string levelToLoad = "level00";
	
	// Use this for initialization
	void Start () {
		GameObject globalVariables = GameObject.FindGameObjectWithTag("Global Variables");
		lp = globalVariables.GetComponent<LoadProperties>();
		bti = globalVariables.GetComponent<BlockTypeInitializer>();		
		
		bti.CopyStartingInventory(lp.levelToLoadNext);
		
		levelToLoad = "level";
		if(lp.levelToLoadNext < 10)
			levelToLoad += "0";
		levelToLoad += lp.levelToLoadNext.ToString();
		
		Application.LoadLevel(levelToLoad);
	}
}
