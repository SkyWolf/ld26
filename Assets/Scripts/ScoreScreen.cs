using UnityEngine;
using System.Collections;

public class ScoreScreen : MonoBehaviour {
	public GUIText time;
	public GUIText next;
	private LoadProperties lp;
	
	// Use this for initialization
	void Start () {
		lp = GameObject.FindGameObjectWithTag("Global Variables").GetComponent<LoadProperties>();
		time.text = "in " + Utilities.TimerToString(lp.previousScore);
		
		if(lp.levelToLoadNext == lp.totalNumberOfLevels - 1)
			next.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Jump") && next.enabled) {
			lp.levelToLoadNext++;
			Application.LoadLevel("adjustGlobal");
		}
		else if(Input.GetButtonDown("Restart")) {
			Application.LoadLevel("adjustGlobal");
		}
		else if(Input.GetButtonDown("Main")) {
			Application.LoadLevel("mainMenu");
		}
	}
	
	
}
