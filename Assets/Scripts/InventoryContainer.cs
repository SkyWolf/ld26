using UnityEngine;
using System.Collections;

public class InventoryContainer : MonoBehaviour {
	private BlockTypeInitializer bti;
	
	public InventorySlot[] inventorySlots;
	
	// Use this for initialization
	void Start () {
		bti = GameObject.FindGameObjectWithTag("Global Variables").GetComponent<BlockTypeInitializer>();
		
		inventorySlots = new InventorySlot[bti.blockTypes.Length];
		for(int i = 0; i < bti.blockTypes.Length; i++)
			inventorySlots[i] = new InventorySlot(bti.blockTypes[i], bti.startingInventory[i]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
