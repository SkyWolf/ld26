using UnityEngine;
using System.Collections;

public class LocalRotationReset : MonoBehaviour {
	private Quaternion targetRotation = Quaternion.Euler(0, 0, 0);
	
	// Update is called once per frame
	void Update () {
		transform.rotation = targetRotation;
	}
}
