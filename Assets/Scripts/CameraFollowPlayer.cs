using UnityEngine;
using System.Collections;

public class CameraFollowPlayer : MonoBehaviour {
	public Transform target;
	
	// Use this for initialization
	void Start () {
		CatchUp();
	}
	
	// Update is called once per frame
	void Update () {
		CatchUp();
	}
	
	void CatchUp() {
		Vector3 newCameraPosition = target.position;
		newCameraPosition.z = transform.position.z;
		
		transform.position = newCameraPosition;
	}
}
