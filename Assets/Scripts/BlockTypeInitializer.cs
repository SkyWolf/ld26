using UnityEngine;
using System.Collections;

public class BlockTypeInitializer : MonoBehaviour {
	public BlockType[] blockTypes;
	public Level[] levels;
	
	public int[] startingInventory;
	
	public void CopyStartingInventory(int index) {
		startingInventory = levels[index].startingblocks;
	}
}