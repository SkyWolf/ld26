using UnityEngine;
using System.Collections;

[System.Serializable]
public class Level {
	public string name;
	public int[] startingblocks;
}