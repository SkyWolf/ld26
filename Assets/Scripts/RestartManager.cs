using UnityEngine;
using System.Collections;

public class RestartManager : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Restart"))
			Restart();
		if(Input.GetButtonDown("Main"))
			MainMenu();
	}
	
	public void Restart() {
		Application.LoadLevel("adjustGlobal");
	}
	
	public void MainMenu() {
		Application.LoadLevel("mainMenu");
	}
}